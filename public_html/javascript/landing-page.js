var screenOrientation = "Portrait";
function getOrientation() {
    screenOrientation = window.innerWidth > window.innerHeight ? "Landscape" : "Portrait";
}
;

// The following code is to handle Nexus5x frame, do not work really much tho

$(document).ready(function () {
    console.log($(window).height())
    console.log($('body').height())
    if($(window).height() < $('body').height()){
        $('#container').css('width', '80%')
        $('#container').css('margin-left', 'auto')
        $('#container').css('margin-right', 'auto')
    }
});
$(window).resize(function () {
    console.log($(window).height())
         console.log($('body').height())
    if($(window).height() < $('body').height()){
        $('#container').css('width', '80%')
        $('#container').css('margin-left', 'auto')
        $('#container').css('margin-right', 'auto')
    }
});


function setBackgroundPicture() {
    var width = $(window).width();
    var height = $(window).height();
    getOrientation();

    if (screenOrientation === "Portrait") {
        if (width <= 500) {
            $("#landing-picture").css("width", "40%");
        } else {
            if ($(window).height() * .8 <= $("#landing-picture").height()) {
                $("#landing-picture").css("max-width", (window).height() * .9 + "px");
            } else {
                $("#landing-picture").css("max-width", "none");
            }
        }
    } else if (screenOrientation === "Landscape") {
        if (width <= 732) {
            $("#landing-picture").css("width", "27%");
        } else {
            if ($(window).height() * .8 <= $("#landing-picture").height()) {
                $("#landing-picture").css("max-width", $(window).height() * .9 + "px");
            } else {
                $("#landing-picture").css("max-width", "none");
            }
        }
    } else {
    }
}